Rails.application.routes.draw do

  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks',
                                    sessions: 'users/sessions',
                                    registrations: 'users/registrations'}
  devise_scope :user do
    get     "/signup",  to: "users/registrations#new"
    get     "/login",   to: "users/sessions#new"
    post    "/login",   to: "users/sessions#create"
    delete  "/logout",   to:"users/sessions#destroy"
  end

  root "static_pages#home"
  get "/help",          to: "static_pages#help"
  get "/about",         to: "static_pages#about"
  get "/contact",       to: "static_pages#contact"
  get "/plivacypolicy", to: "static_pages#plivacypolicy"

  delete "/notifications/destroy", to: "notifications#destroy_all"

  resources :users,                  only: [:show] do
    member do
      get :following, :followers
    end
  end

  resources :microposts,              only: [:new,:show,:create,:destroy] do
    resources :comments,              only: [:create,:destroy]
  end

  resources :relationships,           only: [:create,:destroy]

  resources :notifications,           only: :index

  resources :favorite_relationships,  only: [:create, :destroy]

  resources :searchs,                 only: :index

  resources :password_edits,          only: [:edit,:update]

end
