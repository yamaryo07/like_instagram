require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
     @user = User.new(name: "Example User", nickname: "Example Example",password: "foobar",password_confirmation: "foobar")
   end

   test "should be valid" do
     assert @user.valid?
   end

   test "name should be present" do
     @user.name = " "
     assert_not @user.valid?
   end


   test "name shoule not be too long" do
     @user.name = "a"*51
     assert_not @user.valid?
   end

   test "nickname should be present" do
     @user.nickname = " "
     assert_not @user.valid?
   end


   test "nickname shoule not be too long" do
     @user.nickname = "a"*51
     assert_not @user.valid?
   end









    test "password should be present (noblank)" do
      @user.password = @user.password_confirmation = " "*6
      assert_not @user.valid?
    end

    test "password should have a minimum length" do
      @user.password = @user.password_confirmation = "a"*5
      assert_not @user.valid?
    end

    test "should follow and unfollow a user" do
      michael = users(:michael)
      archer  = users(:archer)
      assert_not michael.following?(archer)
      michael.follow(archer)
      assert michael.following?(archer)
      assert archer.followers.include?(michael)
      michael.unfollow(archer)
      assert_not michael.following?(archer)
    end

end
