require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  include Warden::Test::Helpers

  test "invalid signup information" do
    get new_user_registration_path
    assert_template "devise/registrations/new"
    assert_no_difference "User.count" do
      post user_registration_path,params: {user:{name: "",
                                            nickname: "",
                                            password: "foo",
                                            password_confirmation: "bar"}}
    end
    assert_template "devise/registrations/new"

  end

  #test "valid signup information" do
    #get new_user_registration_path
    #assert_template "devise/registrations/new"
    #assert_difference "User.count",1 do
    #post user_registration_path,params: {user: {name: "hanahana hanahana",
                                            #nickname: "ははは ははは",
                                            #password: "passowrd",
                                            #password_confirmation: "password"}}
    #end
    #assert_redirected_to root_url
  #end

end
