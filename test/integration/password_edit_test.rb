require 'test_helper'

class PasswordEditTest < ActionDispatch::IntegrationTest
  include Warden::Test::Helpers

  def setup
    @user = users(:michael)
    @other_user = users(:archer)
  end

  test "should redirect edit when not log in" do
     get edit_password_edit_path(@user)
     assert_redirected_to new_user_session_path
  end

  test "unsuccessful edit because current password wrong" do
    login_as(@user,:scope => :user)
    get edit_password_edit_path(@user)
    assert_template 'password_edits/edit'
    patch password_edit_path, params: { user: { current_password:  "",
                                              password: "foofoo",
                                              password_confirmation:"foofoo"
                                              }}
    assert_template 'password_edits/edit'
  end

  test "should redirect edit when logged in as wrong user" do
    login_as(@other_user,:scope => :user)
    get edit_password_edit_path(@user)
    assert_redirected_to root_url
  end

  test "should redirect update when logged in as wrong user" do
    login_as(@other_user,:scope => :user)
    patch password_edit_path(@user), params: { user: { current_password: "password",
                                              password: "foofoo",
                                              password_confirmation: "foofoo"
                                            }}
    assert_redirected_to root_url
  end

  test "successful edit because current password wrong" do
    login_as(@user,:scope => :user)
    get edit_password_edit_path(@user)
    assert_template 'password_edits/edit'
    patch password_edit_path(@user), params: { user: { current_password:  "password",
                                              password: "foofoo",
                                              password_confirmation:"foofoo"
                                              }}
    assert_redirected_to login_path
  end







end
