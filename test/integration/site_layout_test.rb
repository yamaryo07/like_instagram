require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  include Warden::Test::Helpers

  def setup
    @user = users(:michael)
  end

  test "layout links" do
    get root_url
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_url, count: 2
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
    assert_select "a[href=?]", login_path,count: 2
    assert_select "a[href=?]", "/users/auth/facebook"
    assert_select "a[href=?]", signup_path
    get contact_path
    assert_select "title", full_title("お問い合わせ")
    login_as(@user, :scope => :user)
    get root_url
    assert_select "a[href=?]", root_url, count: 2
    assert_select "a[href=?]", notifications_path
    assert_select "a[href=?]", new_micropost_path
    assert_select "a[href=?]", user_path(@user)
    assert_select "a[href=?]", edit_user_registration_path
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
  end
end
