require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  include Warden::Test::Helpers

  def setup
    @user = users(:michael)
  end

  test "login with invalid information" do
    get login_path
    assert_template "devise/sessions/new"
    post login_path,params:{user:{nickname: "",
                                   password: ""}}
    assert_template "devise/sessions/new"
    assert_not flash.empty?
  end

  #test "login with valid information" do
    #get login_path
    #assert_template "devise/sessions/new"
    #post login_path,params:{user:{nickname: @user.nickname,
                                   #password: 'password'}}
    #assert_redirected_to root_url

  #end


end
