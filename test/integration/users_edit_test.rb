require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  include Warden::Test::Helpers

  def setup
    @user = users(:michael)
    @other_user = users(:archer)
  end

  test "unsuccessful edit" do
    login_as(@user,:scope => :user)
    get edit_user_registration_path(@user)
    assert_template 'devise/registrations/edit'
    patch user_registration_path, params: { user: { name:  "",
                                              nickname: ""
                                              }}

    assert_template 'devise/registrations/edit'
  end

  test "successful edit" do
    login_as(@user,:scope => :user)
    get edit_user_registration_path(@user)
    assert_template 'devise/registrations/edit'
    name  = "foo bar"
    nickname = "foo foo"
    patch user_registration_path, params: { user: { name:  name,
                                                    nickname: nickname,
                                                    } }
     assert_not flash.empty?
     assert_redirected_to root_url
     @user.reload
     assert_equal name,  @user.name
     assert_equal nickname, @user.nickname
   end

end
