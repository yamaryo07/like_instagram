# メインのサンプルユーザーを1人作成する
User.create!(name:  "Example User",
             nickname: "Example Example",
             password:              "foobar",
             password_confirmation: "foobar")

# 追加のユーザーをまとめて生成する
99.times do |n|
  name  = Faker::Name.name
  nickname = Faker::Name.name
  password = "password"
  User.create!(name:  name,
               nickname: nickname,
               password:              password,
               password_confirmation: password)
end



# 以下のリレーションシップを作成する
users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }
