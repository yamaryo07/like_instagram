class AddColumnsToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :provider, :string
    add_column :users, :uid, :string
    add_column :users, :name, :string
    add_column :users, :nickname, :string
    add_column :users, :selfintro, :string
    add_column :users, :sex, :integer, default: 0
    add_column :users, :phonenumber, :string
    add_column :users, :image, :string , default: "default_icon.jpg"
    add_column :users, :website, :string
  end
end
