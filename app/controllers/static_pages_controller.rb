class StaticPagesController < ApplicationController

  def home
    if user_signed_in?
      @feed_items = current_user.feed.paginate(page: params[:page])
      @comment = current_user.comments.new
    end
  end

  def help
  end

  def about
  end

  def contact
  end

  def plivacypolicy
    respond_to do |format|
        format.html
        format.js
    end
  end

end
