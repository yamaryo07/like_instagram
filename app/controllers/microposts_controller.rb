class MicropostsController < ApplicationController
  before_action :authenticate_user!
  before_action :correct_user,   only: :destroy

  def new
    @micropost  = current_user.microposts.build
  end

  def show
    @micropost = Micropost.find(params[:id])
    @comments = @micropost.comments.order(created_at: :desc)
    @comment = current_user.comments.new
    respond_to do |format|
        format.html
        format.js
    end
  end

  def create
    @micropost = current_user.microposts.build(micropost_params)
    @micropost.image.attach(params[:micropost][:image])
    if @micropost.save
      flash[:notice] = "投稿しました！"
      redirect_to root_url
    else
      flash[:danger] = "画像が選択されていません。"
      render 'microposts/new'
    end
  end

  def destroy
    @micropost.destroy
    flash[:notice] = "投稿を削除しました"
    redirect_to request.referrer || root_url
  end

  private

    def micropost_params
      params.require(:micropost).permit(:image)
    end

    def correct_user
      @micropost = current_user.microposts.find_by(id: params[:id])
      redirect_to root_url if @micropost.nil?
    end
end
