class PasswordEditsController < ApplicationController
  before_action :authenticate_user!
  before_action :correct_user

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.valid_password?(params[:user][:current_password])
      if params[:user][:password] == params[:user][:password_confirmation]
        if params[:user][:password].size >= 6
          @user.update(user_params)
          redirect_to login_path
        else
          flash[:alert] = "パスワードは6文字以上で設定してください。"
          render "password_edits/edit"
        end
      else
        flash[:alert] = "パスワードが一致しません。"
        render "password_edits/edit"
      end
    else
      flash[:alert] = "現在のパスワードが違います。"
      render "password_edits/edit"
    end

  end

  private
    def user_params
      params.require(:user).permit(:password,:password_confirmation)
    end

    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless @user == current_user
    end

end
