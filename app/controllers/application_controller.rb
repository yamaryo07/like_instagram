class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :search_microposts

  def search_microposts
    @q = Comment.ransack(params[:q])
  end

  protected
    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name,:nickname,:agreement])
      devise_parameter_sanitizer.permit(:account_update, keys: [:name,:nickname,:website,:selfintro,:phonenumber,:sex,:email])
    end
end
