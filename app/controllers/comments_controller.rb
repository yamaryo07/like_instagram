class CommentsController < ApplicationController
  before_action :authenticate_user!

  def create
    @micropost = Micropost.find(params[:micropost_id])
    @comment = @micropost.comments.build(comment_params)
    @comment.user_id = current_user.id
    @comment_micropost = @comment.micropost
      if @comment.save
        flash[:notice] = "コメントしました。"
        @comment_micropost.create_notification_comment!(current_user, @comment.id)
        redirect_to @micropost
      else
        flash[:alert] = "20文字以内で入力しでください。"
        redirect_to @micropost
      end
  end

  private
    def comment_params
      params.require(:comment).permit(:content,:micropost_id)
    end
end
