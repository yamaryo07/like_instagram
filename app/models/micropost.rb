class Micropost < ApplicationRecord
  belongs_to :user
  has_many :comments,               dependent: :destroy
  has_many :notifications,          dependent: :destroy
  has_many :favorite_relationships, dependent: :destroy
  has_many :liked_by,               through: :favorite_relationships, 
                                    source: :user
  validates :user_id,presence: true
  validates :image, content_type: { in: %w[image/jpeg image/png],
                                      message: "must be a valid image format" },
                      size:         { less_than: 5.megabytes,
                                      message: "should be less than 5MB" },
                      presence: true
  has_one_attached :image

  default_scope -> { order(created_at: :desc) }



  def display_image
    image.variant(resize_to_fill: [200, 200])
  end

  def create_notification_by(current_user)
    notification = current_user.active_notifications.new(
      micropost_id: id,
      visited_id: user_id,
      action: "like"
    )
    notification.save if notification.valid?
  end

  def create_notification_comment!(current_user, comment_id)
      temp_ids = Comment.select(:user_id).where(micropost_id: id).where.not(user_id: current_user.id).distinct
      temp_ids.each do |temp_id|
          save_notification_comment!(current_user, comment_id, temp_id['user_id'])
      end
      save_notification_comment!(current_user, comment_id, user_id) if temp_ids.blank?
  end

  def save_notification_comment!(current_user, comment_id, visited_id)
      notification = current_user.active_notifications.new(
        micropost_id: id,
        comment_id: comment_id,
        visited_id: visited_id,
        action: 'comment'
      )
      if notification.visiter_id == notification.visited_id
        notification.checked = true
      end
      notification.save if notification.valid?
   end

end
